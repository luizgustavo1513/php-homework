@extends('default')

@section('content')
<h2>CADASTRO DE PRODUTOS</h2>
 
<br />

<ul>
  @foreach ($products as $product)
  <li>{{ $product->name }}</li>
  @endforeach
</ul>

<br />

<form action="{{ route("product.create") }}" method="post" style="border-top: 1px solid black; padding-top: 16px;">
  @csrf
  @method("POST")
  <fieldset>
    <div class="form-group @if ($errors->has('name')) has-error @endif">
      <label for="nameProduct">Nome:</label>
      <input type="text" id="nameProduct" name="name" class="form-control" value="{{ old("name") }}" />
      @if ($errors->has('name'))
      <span class="help-block" role="alert">
          <strong>{{ $errors->first('name') }}</strong>
      </span>
      @endif
    </div>

    <br />

    <div class="form-group @if ($errors->has('description')) has-error @endif">
      <label for="descriptionProduct">Descricao:</label>
      <input type="text" id="descriptionProduct" name="description" class="form-control" value="{{ old("description") }}" />
      @if ($errors->has('description'))
      <span class="help-block" role="alert">
          <strong>{{ $errors->first('description') }}</strong>
      </span>
      @endif
    </div>

    <br />

    <div class="form-group @if ($errors->has('price')) has-error @endif">
      <label for="priceProduct">Preco:</label>
      <input type="text" id="priceProduct" name="price" class="form-control" value="{{ old("price") }}" />
      @if ($errors->has('price'))
      <span class="help-block" role="alert">
          <strong>{{ $errors->first('price') }}</strong>
      </span>
      @endif
    </div>

    <br />

    <div class="form-group @if ($errors->has('category')) has-error @endif">
      <label for="categoryProduct">Categoria:</label>
      <input type="text" id="categoryProduct" name="category" class="form-control" value="{{ old("category") }}" />
      @if ($errors->has('category'))
      <span class="help-block" role="alert">
          <strong>{{ $errors->first('category') }}</strong>
      </span>
      @endif
    </div>

    <br />

    <button class="btn btn-primary btn-lg">Cadastrar Produto</button>
  </fieldset>
</form>
@endsection
